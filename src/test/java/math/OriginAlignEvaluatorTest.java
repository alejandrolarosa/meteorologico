package math;

import model.WeatherForeCast;
import org.junit.Test;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by alejandrol on 7/28/17.
 */
public class OriginAlignEvaluatorTest {


    @Test
    public void test(){
        final List<CartesianPoint> points = new ArrayList<>();
        points.add(new CartesianPoint(BigInteger.valueOf(500),BigInteger.valueOf(500)));
        points.add(new CartesianPoint(BigInteger.valueOf(1000),BigInteger.valueOf(1000)));
        points.add(new CartesianPoint(BigInteger.valueOf(2000),BigInteger.valueOf(2000)));
        final WeatherForeCast wf = new WeatherForeCast();
        new AreaCalculator(points,wf).calculate();
        assertTrue(wf.getArea().equals(BigInteger.ZERO));
        new AlignPointsEvaluator(points,wf).calculate();
        assertTrue(wf.isAligned());
    }

    @Test
    public void test2(){
        final List<CartesianPoint> points = new ArrayList<>();
        points.add(new CartesianPoint(BigInteger.valueOf(0),BigInteger.valueOf(500)));
        points.add(new CartesianPoint(BigInteger.valueOf(0),BigInteger.valueOf(1000)));
        points.add(new CartesianPoint(BigInteger.valueOf(0),BigInteger.valueOf(2000)));
        final WeatherForeCast wf = new WeatherForeCast();
        new AreaCalculator(points,wf).calculate();
        assertTrue(wf.getArea().equals(BigInteger.ZERO));
        new AlignPointsEvaluator(points,wf).calculate();
        assertTrue(wf.isAligned());
    }

    @Test
    public void test3(){
        final List<CartesianPoint> points = new ArrayList<>();
        points.add(new CartesianPoint(BigInteger.valueOf(500),BigInteger.valueOf(0)));
        points.add(new CartesianPoint(BigInteger.valueOf(1000),BigInteger.valueOf(0)));
        points.add(new CartesianPoint(BigInteger.valueOf(2000),BigInteger.valueOf(0)));
        points.add(new CartesianPoint(BigInteger.valueOf(4000),BigInteger.valueOf(0)));
        final WeatherForeCast wf = new WeatherForeCast();
        new AreaCalculator(points,wf).calculate();
        assertTrue(wf.getArea().equals(BigInteger.ZERO));
        new AlignPointsEvaluator(points,wf).calculate();
        assertTrue(wf.isAligned());
    }

    @Test
    public void test4(){
        final List<CartesianPoint> points = new ArrayList<>();
        points.add(new CartesianPoint(BigInteger.valueOf(12),BigInteger.valueOf(2)));
        points.add(new CartesianPoint(BigInteger.valueOf(24),BigInteger.valueOf(2)));
        points.add(new CartesianPoint(BigInteger.valueOf(36),BigInteger.valueOf(2)));
        final WeatherForeCast wf = new WeatherForeCast();
        new AreaCalculator(points,wf).calculate();
        System.out.println(wf.getArea());
        assertTrue(wf.getArea().equals(BigInteger.ZERO));
        new AlignPointsEvaluator(points,wf).calculate();
        assertTrue(wf.isAligned());
    }

}