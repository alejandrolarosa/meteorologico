package math;

import model.WeatherForeCast;
import org.junit.Test;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by alejandrol on 7/29/17.
 */
public class InteriorPointEvaluatorTest {

    @Test
    public void test(){
        //(0,-1000);, (-1175,1618);, (-475,154);
        //(-195,-460);, (-906,-422);, (1867,716)
        final List<CartesianPoint> points =
                Arrays.asList(new CartesianPoint(BigInteger.valueOf(-195),BigInteger.valueOf(-1)),
                             new CartesianPoint(BigInteger.valueOf(-906),BigInteger.valueOf(-422)),
                            new CartesianPoint(BigInteger.valueOf(1867),BigInteger.valueOf(716)));

        final WeatherForeCast wf = new WeatherForeCast();
         new AreaCalculator(points, wf).calculate();
        assertTrue(wf.getArea().compareTo(BigInteger.ZERO)>0);
        new InteriorPointEvaluator(points, wf).calculate();
        assertTrue(wf.isOriginIn());
    }

    @Test
    public void testNotInternalPoint(){
        //(0,-1000);, (-1175,1618);, (-475,154);
        //(-195,-460);, (-906,-422);, (1867,716)
        final List<CartesianPoint> points =
                Arrays.asList(new CartesianPoint(BigInteger.valueOf(-300),BigInteger.valueOf(100)),
                             new CartesianPoint(BigInteger.valueOf(0),BigInteger.valueOf(500)),
                            new CartesianPoint(BigInteger.valueOf(300),BigInteger.valueOf(100)));
        final WeatherForeCast wf = new WeatherForeCast();
        new AreaCalculator(points,wf).calculate();
        assertTrue(wf.getArea().compareTo(BigInteger.ZERO)>0);
        new InteriorPointEvaluator(points,wf).calculate();
        assertFalse(wf.isOriginIn());
    }



}