package math;

import model.WeatherForeCast;
import org.junit.Test;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by alejandrol on 7/28/17.
 */
public class AreaEvaluatorTest {

    @Test
    public void test(){
        /*
        Planet.move => (996,87);
        Planet.move => (1997,-104);
        Planet.move => (499,-8);
         */
        final List<CartesianPoint> list = new ArrayList<>();
        list.add(new CartesianPoint(BigInteger.valueOf(996), BigInteger.valueOf(87)));
        list.add(new CartesianPoint(BigInteger.valueOf(1997), BigInteger.valueOf(-104)));
        list.add(new CartesianPoint(BigInteger.valueOf(499), BigInteger.valueOf(-8)));

        final WeatherForeCast wf = new WeatherForeCast();
        new AreaCalculator(list,wf).calculate();
//        System.out.println(resultArea);
        assertTrue(wf.getArea().equals(BigInteger.valueOf(190022)));

    }

    @Test
    public void testAreaEqualsZero(){
        /*
        Planet.move => (996,87);
        Planet.move => (1997,-104);
        Planet.move => (499,-8);
         */
        final List<CartesianPoint> list = new ArrayList<>();
        list.add(new CartesianPoint(BigInteger.valueOf(0), BigInteger.valueOf(1000)));
        list.add(new CartesianPoint(BigInteger.valueOf(0), BigInteger.valueOf(200)));
        list.add(new CartesianPoint(BigInteger.valueOf(0), BigInteger.valueOf(5000)));

        final WeatherForeCast wf = new WeatherForeCast();
        new AreaCalculator(list,wf).calculate();
        assertTrue(wf.getArea().equals(BigInteger.ZERO));

    }

    @Test
    public void testAreaEqualsZero2(){
        /*
        Planet.move => (996,87);
        Planet.move => (1997,-104);
        Planet.move => (499,-8);
         */
        final List<CartesianPoint> list = new ArrayList<>();
        list.add(new CartesianPoint(BigInteger.valueOf(1000), BigInteger.valueOf(10)));
        list.add(new CartesianPoint(BigInteger.valueOf(100), BigInteger.valueOf(10)));
        list.add(new CartesianPoint(BigInteger.valueOf(1000), BigInteger.valueOf(10)));

        final WeatherForeCast wf = new WeatherForeCast();
        new AreaCalculator(list,wf).calculate();
        assertTrue(wf.getArea().equals(BigInteger.ZERO));

    }

}