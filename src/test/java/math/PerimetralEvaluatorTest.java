package math;

import model.WeatherForeCast;
import org.junit.Test;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by alejandrol on 7/28/17.
 */
public class PerimetralEvaluatorTest {

    @Test
    public void test(){
        /*
        Planet.move => (996,87);
        Planet.move => (1997,-104);
        Planet.move => (499,-8);
         */
        final List<CartesianPoint> list = new ArrayList<>();
        list.add(new CartesianPoint(BigInteger.valueOf(996), BigInteger.valueOf(87)));
        list.add(new CartesianPoint(BigInteger.valueOf(1997), BigInteger.valueOf(-104)));
        list.add(new CartesianPoint(BigInteger.valueOf(499), BigInteger.valueOf(-8)));
        final WeatherForeCast wf = new WeatherForeCast();
        new PerimeterCalculator(list,wf).calculate();
        System.out.println(wf.getPerimeter());
        assertTrue(wf.getPerimeter().equals(BigInteger.valueOf(3025)));

    }

}