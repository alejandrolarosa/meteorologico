package math;

import model.WeatherForeCast;
import org.junit.Test;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by alejandrol on 8/1/17.
 */
public class AlignPointsWithoutOriginEvaluatorTest {

    @Test
    public void test(){
        final List<CartesianPoint> points = new ArrayList<>();
        points.add(CartesianPoint.origin());
        points.add(new CartesianPoint(BigInteger.valueOf(500),BigInteger.valueOf(500)));
        points.add(new CartesianPoint(BigInteger.valueOf(1000),BigInteger.valueOf(1000)));
        points.add(new CartesianPoint(BigInteger.valueOf(2000),BigInteger.valueOf(2000)));
        final WeatherForeCast wf = new WeatherForeCast();
        new AreaCalculator(points,wf).calculate();
        assertTrue(wf.getArea().equals(BigInteger.ZERO));
        new AlignPointsEvaluator(points,wf).calculate();
        assertTrue(wf.isAligned());
    }

    @Test
    public void test2(){
        final List<CartesianPoint> points = new ArrayList<>();
        points.add(CartesianPoint.origin());
        points.add(new CartesianPoint(BigInteger.valueOf(400),BigInteger.valueOf(500)));
        points.add(new CartesianPoint(BigInteger.valueOf(1000),BigInteger.valueOf(1000)));
        points.add(new CartesianPoint(BigInteger.valueOf(2000),BigInteger.valueOf(2000)));
        final WeatherForeCast wf = new WeatherForeCast();
        new AlignPointsEvaluator(points,wf).calculate();
        assertFalse(wf.isAligned());
    }
}