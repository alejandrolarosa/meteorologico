package runner;

import model.WeatherForeCast;
import model.SolarSystem;
import org.junit.Test;

import java.math.BigInteger;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

/**
 * Created by alejandrol on 7/28/17.
 */
public class RunnerTest {

    @Test
    public void test(){
        final AtomicInteger droughtPeriod = new AtomicInteger();
        final AtomicInteger rainPeriod = new AtomicInteger();
        final AtomicInteger optimalPeriod = new AtomicInteger();
        BigInteger maxPerimeter = BigInteger.ZERO;
        final List<WeatherForeCast> results = SolarSystem.run(1200);
        for(final WeatherForeCast r : results){
            if(r.isOriginIn()) rainPeriod.incrementAndGet();
            if(r.isAligned()) optimalPeriod.incrementAndGet();
            if(r.isAlignedWO()) droughtPeriod.incrementAndGet();
            else if(r.getPerimeter().compareTo(maxPerimeter)>0 && r.isOriginIn()){
                maxPerimeter = r.getPerimeter();
            }
        }
        System.out.println("Períodos de sequía: " + droughtPeriod.intValue());
        System.out.println("Períodos de lluvia: " + rainPeriod.intValue());
        System.out.println("Períodos de condiciones óptimas de presión y temperatura: " + optimalPeriod.intValue());
        final BigInteger finalMaxPerimeter = maxPerimeter;
        final Stream<WeatherForeCast> resultStream = results.stream().filter(r -> r.getPerimeter().equals(finalMaxPerimeter));
        System.out.println("Dia de pico máximo de lluvia: " + resultStream.findFirst().get().getDay());


    }
}