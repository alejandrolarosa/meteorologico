package clustering;

import cache.WeatherForecastCache;
import model.WeatherForeCast;
import model.SolarSystem;
import model.WeatherStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.filters.Filter;
import weka.filters.unsupervised.instance.NonSparseToSparse;

import java.io.File;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

/**
 * Created by alejandrol on 7/30/17.
 */
public class ARFFileBuilder {

    private static final Logger logger = LoggerFactory.getLogger(ARFFileBuilder.class);
    private static final int TIMES = 4000;

    private ARFFileBuilder() {}

    public static String build() throws Exception
      {
          final ArrayList<Attribute> attributes = new ArrayList<>();

          attributes.add(new Attribute("area"));
          attributes.add(new Attribute("perimeter"));
          attributes.add(new Attribute("originIn"));
          attributes.add(new Attribute("AlignWOOrigin"));
          attributes.add(new Attribute("Aligned"));

          final Instances dataSet = new Instances("ESDN", attributes, 0);

          final List<WeatherForeCast> results = SolarSystem.run(TIMES);
          final AtomicInteger droughtPeriod = new AtomicInteger();
          final AtomicInteger rainPeriod = new AtomicInteger();
          final AtomicInteger optimalPeriod = new AtomicInteger();
          BigInteger maxPerimeter = BigInteger.ZERO;

          for(final WeatherForeCast r : results){
              if(r.isOriginIn()) rainPeriod.incrementAndGet();
              if(r.isAligned()) optimalPeriod.incrementAndGet();
              if(r.isAlignedWO()) droughtPeriod.incrementAndGet();
              else if(r.getPerimeter().compareTo(maxPerimeter)>0 && r.isOriginIn()){
                maxPerimeter = r.getPerimeter();
              }
              final double[] values = new double[dataSet.numAttributes()];
              values[0] = r.getArea().doubleValue();
              values[1] = r.getPerimeter().doubleValue();
              values[2] = r.isOriginIn()?1:0;
              values[3] = r.isAlignedWO()?1:0;
              values[4] = r.isAligned()?1:0;
              dataSet.add(new DenseInstance(1.0, values));
          }
          final BigInteger finalMaxPerimeter = maxPerimeter;
          results.stream()
                  .filter(r -> r.getPerimeter().equals(finalMaxPerimeter))
                  .forEach(md -> WeatherForecastCache.put(md.getDay(), WeatherStatus.STORM));
          sumary(results, rainPeriod, optimalPeriod, maxPerimeter);
          final NonSparseToSparse nonSparseToSparseInstance = new NonSparseToSparse();
          nonSparseToSparseInstance.setInputFormat(dataSet);
          final Instances sparseDataset = Filter.useFilter(dataSet, nonSparseToSparseInstance);

          logger.info(sparseDataset.toString());

          final ArffSaver arffSaverInstance = new ArffSaver();
          arffSaverInstance.setInstances(sparseDataset);
          final File planetsPosition = File.createTempFile("planetsPosition", ".arff");
          arffSaverInstance.setFile(planetsPosition);
          arffSaverInstance.writeBatch();
          return planetsPosition.getAbsolutePath();
       }

    private static void sumary(List<WeatherForeCast> results, AtomicInteger rainPeriod, AtomicInteger optimalPeriod, BigInteger maxPerimeter) {

        logger.info("Períodos de sequía: " + WeatherForecastCache.countDrought());
        logger.info("Períodos de lluvia: " + rainPeriod.intValue());
        logger.info("Períodos de condiciones óptimas de presión y temperatura: " + optimalPeriod.intValue());
        final BigInteger finalMaxPerimeter = maxPerimeter;
        final Optional<WeatherForeCast> first = results.stream().filter(r -> r.getPerimeter().equals(finalMaxPerimeter)).findFirst();
        first.ifPresent(weatherForeCast -> logger.info("Dia de pico máximo de lluvia: " + weatherForeCast.getDay()));
    }
}
