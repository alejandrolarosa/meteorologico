package clustering;

import cache.ProbabilisticWeatherForecastCache;
import model.WeatherStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import weka.clusterers.ClusterEvaluation;
import weka.clusterers.DensityBasedClusterer;
import weka.clusterers.EM;
import weka.core.Instances;

import java.io.BufferedReader;
import java.io.FileReader;

/**
 * An example class that shows the use of Weka clusterers from Java.
 *
 * @author  FracPete
 */

public class ClusterWeather {

  private static final Logger logger = LoggerFactory.getLogger(ClusterWeather.class);
  private static final String OF_CLUSTERS = "# of clusters: ";

  private ClusterWeather(){}

      public static void execute(String filename) throws Exception {
        final Instances data = new Instances(new BufferedReader(new FileReader(filename)));
          // normal
        logger.info("\n--> normal");
        final String[] options = new String[2];
        options[0] = "-t";
        options[1] = filename;
        logger.info(ClusterEvaluation.evaluateClusterer(new EM(), options));

        // manual call
        logger.info("\n--> manual");
        final DensityBasedClusterer cl = new EM();
        cl.buildClusterer(data);
        final ClusterEvaluation eval = new ClusterEvaluation();
        eval.setClusterer(cl);
        eval.evaluateClusterer(new Instances(data));
        logger.info(OF_CLUSTERS + eval.getNumClusters());
        for (int i = 0; i < eval.getClusterAssignments().length; i++) {
          switch ((int) eval.getClusterAssignments()[i]){
            case 0 :
            case 3 : ProbabilisticWeatherForecastCache.put(i, WeatherStatus.RAIN);
                     break;
            case 1 : ProbabilisticWeatherForecastCache.put(i,WeatherStatus.OPTIMAL_PRESSURE_AND_TEMPERATURE);
                     break;
            case 2 :
            case 4 : ProbabilisticWeatherForecastCache.put(i,WeatherStatus.CLEAR);
                     break;
          }
        }
        // density based
        logger.info("\n--> density (CV)");
        final DensityBasedClusterer em = new EM();
        final ClusterEvaluation clusterEvaluation = new ClusterEvaluation();
        clusterEvaluation.setClusterer(em);
        ClusterEvaluation.crossValidateModel(em, data, 10, data.getRandomNumberGenerator(1));
        logger.info(OF_CLUSTERS + clusterEvaluation.getNumClusters());
      }

}
