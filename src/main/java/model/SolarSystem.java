package model;

import cache.WeatherForecastCache;
import cache.WeatherStatusNotFoundException;
import concurrency.EvaluatorCallable;
import math.CartesianPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by alejandrol on 7/28/17.
 */
public class SolarSystem {

    private static final Logger logger = LoggerFactory.getLogger(SolarSystem.class);

    private static final List<Planet> planets= new ArrayList<Planet>();
    private static final int NTHREDS = 20;


    static{
        final Planet vulcano = new Planet("Vulcano", 5, 1000, false);
        final Planet betasoide = new Planet("Betasoide", 3, 2000, true);
        final Planet ferengi = new Planet("Ferengi", 1, 500, true);
        addPlanet(vulcano);
        addPlanet(betasoide);
        addPlanet(ferengi);
    }
    private SolarSystem() {}

    /**
     * @param planet to add system
     */
    private static void addPlanet(Planet planet){
        planets.add(planet);
    }


    /**
     * @param times period to move solar system.
     * @return List of result.
     */
    public static List<WeatherForeCast> run(int times){
        planets.sort(Comparator.comparingInt(Planet::getSunDistance));
        final ExecutorService executor = Executors.newFixedThreadPool(NTHREDS);
        final List<Future<WeatherForeCast>> list = new ArrayList<Future<WeatherForeCast>>();
        IntStream.range(0,times).forEach(i -> {
            final Stream<CartesianPoint> cartesianCoordinates = planets.stream().map(Planet::move);
            final List<CartesianPoint> points = cartesianCoordinates.collect(Collectors.toList());
            final Callable<WeatherForeCast> worker = new EvaluatorCallable(i,points);
            final Future<WeatherForeCast> submit = executor.submit(worker);
            list.add(submit);

        });
        final List<WeatherForeCast> results = new ArrayList<>();
        for (final Future<WeatherForeCast> future : list) {
           try {
               final WeatherForeCast deterministicWeather = future.get();
               results.add(deterministicWeather);
               WeatherForecastCache.put(deterministicWeather.getDay(),deterministicWeather.weatherIs());
           } catch (final InterruptedException | ExecutionException e) {
               logger.warn("Cannot add some result",e);
           } catch (final WeatherStatusNotFoundException e) {
               //do nothing
           }
        }
        executor.shutdown();
        return results;
    }

}
