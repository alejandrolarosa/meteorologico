package model;

import cache.WeatherStatusNotFoundException;

import java.math.BigInteger;

/**
 * Created by alejandrol on 7/28/17.
 */
public class WeatherForeCast {

    private BigInteger perimeter;
    private BigInteger area = BigInteger.ZERO;
    private boolean originIn;
    private boolean alignWithOrigin;
    private boolean aligned;
    private int day;

    public WeatherForeCast withPerimeter(BigInteger perimeter){
        this.perimeter = perimeter;
        return this;
    }

    public WeatherForeCast withOriginIn(boolean b){
        originIn = b;
        return this;
    }

    public WeatherForeCast withArea(BigInteger b){
        area = b;
        return this;
    }

    public WeatherForeCast alignWithOrigin(boolean b){
        alignWithOrigin = b;
        return this;
    }

    public WeatherForeCast aligned(boolean b){
        aligned = b;
        return this;
    }

    public WeatherForeCast withDay(int d){
        day = d;
        return this;
    }

    @Override
    public String toString() {
        return "(day:"+ day + "; perimeter: " + perimeter + "; area: " + area + "; originIn: " + originIn +"; alignWithOrigin: " + alignWithOrigin + ")";
    }

    public int getDay() {
        return day;
    }

    public BigInteger getPerimeter() {
        return perimeter;
    }

    public boolean isOriginIn() {
        return originIn;
    }

    public BigInteger getArea() {
        return area;
    }

    public boolean isAlignedWO() {
        return alignWithOrigin;
    }

    public boolean isAligned() {
        return aligned && !alignWithOrigin;
    }

    public WeatherStatus weatherIs() throws WeatherStatusNotFoundException {
        if(isAligned()){
            return WeatherStatus.OPTIMAL_PRESSURE_AND_TEMPERATURE;
        }
        if(isAlignedWO()){
            return WeatherStatus.DROUGHT;
        }
        throw new WeatherStatusNotFoundException();
    }

}
