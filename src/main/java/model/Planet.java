package model;

import math.CartesianPoint;

import static web.json.JsonUtils.toJson;

/**
 * Created by alejandrol on 7/28/17.
 */

class Planet {

    private final String name;
    private final int speed;
    private final int sunDistance;
    private final boolean clockWise;
    private int counter;

    /**
     * @param name
     * @param speed
     * @param sunDistance
     * @param clockWise
     */
    public Planet(String name,int speed,int sunDistance,boolean clockWise) {
        this.name = name;
        this.speed=speed;
        this.sunDistance=sunDistance;
        this.clockWise =clockWise;
        counter = 0;
    }

    /**
     * @return Cartesian coordinate when move planet 1 time.
     */
    public CartesianPoint move(){
        counter +=1;
        final int degrees = clockWise ? -1 * speed * counter :  speed * counter;
        return new CartesianPoint(degrees, sunDistance);
    }

    public int getSunDistance(){ return sunDistance;}

    @Override
    public String toString() {
        return toJson(this);
    }
}
