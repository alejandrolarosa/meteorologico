package model;

/**
 * Created by alejandrol on 8/1/17.
 */
public enum WeatherStatus {

    STORM,
    RAIN,
    OPTIMAL_PRESSURE_AND_TEMPERATURE,
    DROUGHT,
    CLEAR
}
