package web;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import static web.json.JsonUtils.toJson;

/**
 * Created by alejandrol on 7/31/17.
 */
public class WeatherResponse {

    @Expose
    @SerializedName("dia")
    private final String day;

    @Expose
    @SerializedName("clima")
    private final String value;

    public static WeatherResponse create(String day, String value){
        return new WeatherResponse(day,value);
    }

    private WeatherResponse(String day, String value) {
        this.day = day;
        this.value = value;
    }

    public static WeatherResponse notFound(String day){
        return create(day,"No informado");
    }

    public static WeatherResponse invalidDay(String day){
        return create(day,"No informado por dia invalido");
    }

    @Override
    public String toString() {
           return toJson(this);
       }

}
