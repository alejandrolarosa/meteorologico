package web;

import jobs.Job;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import web.controllers.WeatherController;

import static spark.Spark.port;

/**
 * Start application web
 */
public class StartApp {


    private static final Logger logger = LoggerFactory.getLogger(StartApp.class);
    private StartApp() {}

    public static void main(String[] args) throws Exception {
        port(8080);
        if(args.length > 0){
            logger.info("Web app use following port:"+ args[0]);
            port(Integer.valueOf(args[0]));
        }
        new WeatherController();
        Job.start();

    }


}
