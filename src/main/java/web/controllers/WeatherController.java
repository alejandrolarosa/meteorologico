package web.controllers;
import cache.WeatherForecastCache;
import cache.ProbabilisticWeatherForecastCache;
import cache.WeatherFinder;
import cache.WeatherStatusNotFoundException;
import model.WeatherStatus;
import web.WeatherResponse;

import static web.json.JsonUtils.json;
import static spark.Spark.get;


public class WeatherController {


	public WeatherController(){
        final WeatherFinder weatherCache = new ProbabilisticWeatherForecastCache(new WeatherForecastCache());
        get("/clima", "application/json",(req, res) -> {
            final String day = req.queryParams("dia");
            try{
                final Integer d = Integer.valueOf(day);
                final WeatherStatus status = weatherCache.weatherIs(d);
                return WeatherResponse.create(day,status.name());
            }catch(final WeatherStatusNotFoundException e){
                return WeatherResponse.notFound(day);
            }catch(final NumberFormatException e){
                return WeatherResponse.invalidDay(day);
            }

        }, json());
    }
}
