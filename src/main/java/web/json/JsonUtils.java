package web.json;

import com.google.gson.GsonBuilder;
import spark.ResponseTransformer;

/**
 * Created by alejandrol on 4/27/17.
 */
public class JsonUtils {

    private JsonUtils() {}

    public static String toJson(Object object) {
        final GsonBuilder gson = new GsonBuilder();
        gson.excludeFieldsWithoutExposeAnnotation();
        return gson.create().toJson(object);
    }

    public static ResponseTransformer json() {
        return JsonUtils::toJson;
    }
}
