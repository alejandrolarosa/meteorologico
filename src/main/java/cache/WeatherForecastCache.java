package cache;

import model.WeatherStatus;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by alejandrol on 7/31/17.
 */
public class WeatherForecastCache implements WeatherFinder{


    private static final Map<Integer, WeatherStatus> weather = new HashMap<>();

    public static void put(Integer key,WeatherStatus value){
        weather.put(key,value);
    }

    public WeatherStatus weatherIs(int day) throws WeatherStatusNotFoundException {
       if(weather.get(day)== null){
           throw new WeatherStatusNotFoundException();
       }
       return weather.get(day);

    }

    public static long countDrought() {
        return weather.entrySet().stream()
                        .filter(w -> w.getValue() == WeatherStatus.DROUGHT).count();
    }
}
