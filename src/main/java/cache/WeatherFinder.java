package cache;

import model.WeatherStatus;

/**
 * Created by alejandrol on 8/1/17.
 */
public interface WeatherFinder {


    WeatherStatus weatherIs(int day) throws WeatherStatusNotFoundException;
}
