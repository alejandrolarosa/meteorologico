package cache;

/**
 * Created by alejandrol on 8/1/17.
 */
public class WeatherStatusNotFoundException extends Exception {

    private static final long serialVersionUID = -3128451522553508671L;

    public WeatherStatusNotFoundException() {
        super("Weather not found");
    }
}
