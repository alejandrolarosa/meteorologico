package cache;

import model.WeatherStatus;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by alejandrol on 7/31/17.
 */
public class ProbabilisticWeatherForecastCache extends FinderDecorator{


    private static final Map<Integer, WeatherStatus> weather = new HashMap<>();

    public ProbabilisticWeatherForecastCache(WeatherFinder finder) {
        super(finder);
    }

    public static void put(Integer key,WeatherStatus value){
        weather.put(key, value);
    }

    @Override
    public WeatherStatus weatherIs(int day) throws WeatherStatusNotFoundException {
        try{
            return super.finder.weatherIs(day);
        }catch(final WeatherStatusNotFoundException e){
            final WeatherStatus weatherStatus = weather.get(day);
            if(weatherStatus== null) throw new WeatherStatusNotFoundException();

            return weatherStatus;
        }

    }
}
