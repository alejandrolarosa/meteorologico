package cache;


import model.WeatherStatus;

/**
 * Created by alejandrol on 8/1/17.
 */
public class FinderDecorator implements WeatherFinder {

    WeatherFinder finder;

    FinderDecorator(WeatherFinder finder){
        this.finder = finder;
    }

    @Override
    public WeatherStatus weatherIs(int day) throws WeatherStatusNotFoundException {
        return finder.weatherIs(day);
    }

}
