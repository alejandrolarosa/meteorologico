package concurrency;

import math.*;
import math.AlignPointsEvaluator;
import math.AreaCalculator;
import math.InteriorPointEvaluator;
import math.PerimeterCalculator;
import model.WeatherForeCast;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by alejandrol on 7/31/17.
 */
public class EvaluatorCallable implements Callable<WeatherForeCast> {

    private final int time;
    private final List<CartesianPoint> points= new ArrayList<>();

    public EvaluatorCallable(int time, List<CartesianPoint> points) {
        this.time = time;
        this.points.addAll(points);
    }

    @Override
    public WeatherForeCast call() throws Exception {



        final WeatherForeCast wf = new WeatherForeCast();
        final EvaluationStrategy[] algorithms = {new AreaCalculator(points,wf),
                                       new AlignPointsEvaluator(points,wf),
                                        new InteriorPointEvaluator(points,wf),
                                        new PerimeterCalculator(points,wf),
                                        new AlignPointsWithOriginEvaluator(points,wf)};
        int index = 0;
        while(index < algorithms.length){
            algorithms[index].calculate();
            index +=1;
        }
        wf.withDay(time +1);
        return wf;
    }
}
