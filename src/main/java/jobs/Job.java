package jobs;

import clustering.ARFFileBuilder;
import clustering.ClusterWeather;

/**
 * Created by alejandrol on 7/30/17.
 */
public class Job {

    private Job() {}

    public static void start() throws Exception {
        final String pathARFFile = ARFFileBuilder.build();
        ClusterWeather.execute(pathARFFile);
    }
}
