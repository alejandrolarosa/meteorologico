package math;

import model.WeatherForeCast;

import java.util.List;

/**
 * Created by alejandrol on 8/3/17.
 */
public class InteriorPointEvaluator extends Evaluator implements EvaluationStrategy {

    public InteriorPointEvaluator(List<CartesianPoint> points, WeatherForeCast weatherForeCast){
        super(points,weatherForeCast);
    }


    @Override
    public WeatherForeCast calculate() {
        return weatherForeCast.withOriginIn(evaluate(points));
    }

    /**
         * @param poly list of points
         * @return true if center of coordinates between all points.
         */
    private boolean evaluate(List<CartesianPoint> poly){
        //this method uses the ray tracing algorithm to determine if the point is in the polygon
        final int nPoints=poly.size();
        boolean locatedInPolygon=false;
        for(int i = 0; i<(nPoints); i++){
            //repeat loop for all sets of points
            final int j;
            if(i==(nPoints-1)){
                //if i is the last vertex, let j be the first vertex
                j= 0;
            }else{
                //for all-else, let j=(i+1)th vertex
                j=i+1;
            }

            final float vertY_i= poly.get(i).y().floatValue();
            final float vertX_i= poly.get(i).x().floatValue();
            final float vertY_j= poly.get(j).y().floatValue();
            final float vertX_j= poly.get(j).x().floatValue();
            final float testY  = 0;

            // following statement checks if testPoint.Y is below Y-coord of i-th vertex
            final boolean belowLowY=vertY_i>testY;
            // following statement checks if testPoint.Y is below Y-coord of i+1-th vertex
            final boolean belowHighY=vertY_j>testY;

            /* following statement is true if testPoint.Y satisfies either (only one is possible)
            -->(i).Y < testPoint.Y < (i+1).Y        OR
            -->(i).Y > testPoint.Y > (i+1).Y

            (Note)
            Both of the conditions indicate that a point is located within the edges of the Y-th coordinate
            of the (i)-th and the (i+1)- th vertices of the polygon. If neither of the above
            conditions is satisfied, then it is assured that a semi-infinite horizontal line draw
            to the right from the testpoint will NOT cross the line that connects vertices i and i+1
            of the polygon
            */
            final boolean withinYsEdges= belowLowY != belowHighY;

            if( withinYsEdges){
                // this is the slope of the line that connects vertices i and i+1 of the polygon
                final float slopeOfLine   = ( vertX_j-vertX_i )/ (vertY_j-vertY_i) ;

                // this looks up the x-coord of a point lying on the above line, given its y-coord
                final float pointOnLine   = ( slopeOfLine* (testY - vertY_i) )+vertX_i;

                //checks to see if x-coord of testPoint is smaller than the point on the line with the same y-coord
                final float testX = 0;
                final boolean isLeftToLine= testX < pointOnLine;

                if(isLeftToLine){
                    //this statement changes true to false (and vice-versa)
                    locatedInPolygon= !locatedInPolygon;
                }//end if (isLeftToLine)
            }//end if (withinYsEdges
        }

        return locatedInPolygon;
    }
}