package math;

import model.WeatherForeCast;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by alejandrol on 8/3/17.
 */
public class PerimeterCalculator extends Evaluator implements EvaluationStrategy {

    public PerimeterCalculator(List<CartesianPoint> points, WeatherForeCast weatherForeCast){
        super(points,weatherForeCast);
    }

    private BigInteger loop(int index,List<CartesianPoint> points, BigInteger result) {
        final int last = points.size() - 1;
        if(index== last) {
            return result.add(points.get(0).distance(points.get(last)));
        }
        final BigInteger distance = points.get(index).distance(points.get(index+1));
        return loop(index+1,points,distance.add(result));
    }

    @Override
    public WeatherForeCast calculate() {
        return weatherForeCast.withPerimeter(loop(0,points, BigInteger.ZERO));
    }
}