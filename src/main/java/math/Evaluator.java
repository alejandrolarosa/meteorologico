package math;

import math.CartesianPoint;
import model.WeatherForeCast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alejandrol on 8/3/17.
 */
public abstract class Evaluator {

    protected List<CartesianPoint> points= new ArrayList<>();
    WeatherForeCast weatherForeCast;

    public Evaluator(List<CartesianPoint> points, WeatherForeCast weatherForeCast){
        this.points.addAll(points);
        this.weatherForeCast = weatherForeCast;
    }

}
