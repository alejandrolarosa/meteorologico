package math;

import math.CartesianPoint;
import model.WeatherForeCast;

import java.util.List;

/**
 * Created by alejandrol on 8/3/17.
 */
public interface EvaluationStrategy {

    WeatherForeCast calculate();

}
