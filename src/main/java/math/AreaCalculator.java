package math;

import model.WeatherForeCast;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alejandrol on 8/3/17.
 */
public class AreaCalculator extends Evaluator implements EvaluationStrategy {

    public AreaCalculator(List<CartesianPoint> points, WeatherForeCast weatherForeCast){
        super(points,weatherForeCast);
    }

    private BigInteger loop(List<CartesianPoint> pts, BigInteger left, BigInteger right) {
        if(pts.size()==1) return right.add(left.negate()).abs();
        final CartesianPoint a = pts.get(0);
        final CartesianPoint b = pts.get(1);
        pts.remove(0);
        return loop(pts,left.add(a.productXY(b)),right.add(a.productYX(b)));
    }

    @Override
    public WeatherForeCast calculate() {
        final List<CartesianPoint> newList = new ArrayList<>(points);
        newList.add(points.get(0));
        return weatherForeCast.withArea(loop(newList, BigInteger.ZERO,BigInteger.ZERO));
    }
}