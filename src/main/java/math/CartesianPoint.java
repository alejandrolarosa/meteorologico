package math;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Created by alejandrol on 7/28/17.
 */
public class CartesianPoint {

    private static final CartesianPoint origin= new CartesianPoint(BigInteger.ZERO,BigInteger.ZERO);
    private final BigInteger x;
    private final BigInteger y;


    /**
     * @param degrees degree to convert
     * @param ratio      ratio of circle
     */
    public CartesianPoint(int degrees, int ratio) {
        final double radian = (Math.PI / 180) * degrees;
        x = BigDecimal.valueOf(ratio * Math.cos(radian)).toBigInteger();
        y = BigDecimal.valueOf(ratio * Math.sin(radian)).toBigInteger();
    }

    /**
     * @param x value of x element
     * @param y value of y element
     */
    public CartesianPoint(BigInteger x, BigInteger y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "("+x+","+y+");";
    }

    /**
     * @return value of x
     */
    public BigInteger x(){return x;}

    /**
     * @return value of x
     */
    public BigInteger y(){return y;}

    /**
     * @return center of cartesian coordinates
     */
    public static CartesianPoint origin(){ return origin;}


    public BigInteger productXY(CartesianPoint b){
        return x().multiply(b.y());
    }

    public BigInteger productYX(CartesianPoint b){
        return y().multiply(b.x());
    }

    public BigInteger distance(CartesianPoint b) {
        return BigInteger.valueOf((long) Math.sqrt(Math.pow(b.x().add(x().negate()).doubleValue(), 2)+Math.pow(b.y().add(y().negate()).doubleValue(), 2)));
    }
}
