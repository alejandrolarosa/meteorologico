package math;

import model.WeatherForeCast;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alejandrol on 8/3/17.
 */
public class AlignPointsWithOriginEvaluator extends AlignPointsEvaluator {

    public AlignPointsWithOriginEvaluator(List<CartesianPoint> points, WeatherForeCast weatherForeCast) {
        super(points,weatherForeCast);
        final List<CartesianPoint> pointsWithOrigin = new ArrayList<>(points);
        super.points.clear();
        super.points.add(CartesianPoint.origin());
        super.points.addAll(pointsWithOrigin);

    }

    @Override
    public WeatherForeCast calculate() {
        if(points.isEmpty() || points.size() < 3) return weatherForeCast.aligned(true);
        final CartesianPoint first = points.get(0);
        final CartesianPoint last = points.get(points.size()-1);
        final BigInteger totalDistance = loop(0, points, BigInteger.ZERO);
        return weatherForeCast.alignWithOrigin(weatherForeCast.isAligned() && first.distance(last).equals(totalDistance));
    }

}
