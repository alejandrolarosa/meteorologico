package math;

import model.WeatherForeCast;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by alejandrol on 8/3/17.
 */
public class AlignPointsEvaluator extends Evaluator implements EvaluationStrategy {

    public AlignPointsEvaluator(List<CartesianPoint> points, WeatherForeCast weatherForeCast) {
        super(points,weatherForeCast);
    }

    @Override
    public WeatherForeCast calculate() {
        if(points.isEmpty() || points.size() < 3) return weatherForeCast.aligned(true);
        final CartesianPoint first = points.get(0);
        final CartesianPoint last = points.get(points.size()-1);
        final BigInteger totalDistance = loop(0, points, BigInteger.ZERO);
        return  weatherForeCast.aligned(first.distance(last).equals(totalDistance));
    }

    BigInteger loop(int index, List<CartesianPoint> ps, BigInteger total) {
       if(index == ps.size()-1) return total;
       return loop(index+1,ps, total.add(ps.get(index).distance(ps.get(index+1))));
   }
}
