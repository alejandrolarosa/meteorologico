# Sistema metereológico
La solucion se realiza utilizando las siguientes tecnologias.
  - spark java (capa web)
  - Google gson (json)
  - junit
  - Weka (clusterizar datos no deterministicos)

# Start App
  - Debemos hacer git clone del proyecto.
  - git clone https://gitlab.com/alejandrolarosa/meteorologico.git
  - Luego compilar y generar el artifact
```
$ cd meteorologico
$ mvn clean install
$ ./start.sh
```

Para la solucion tenemos en el modelo de dominio las clases Planet, 
SolarSystem, WeatherForeCast y el enum WeatherStatus.
Dependiendo de la cantidad de periodos que queremos mover el sistema solar
vamos a obtener por cada movimiento un punto en R2 (x,y) por el movimiento de cada planeta. 
Con el conjunto de puntos vamos a calcular Perimetro, Area y distancia entre los puntos. 
Con esos valores construimos vectores que luego con Weka los clusterizamos para los casos
que no tenemos una logica que nos permita determinar que clima tiene ese dia.
Estos resultados son guardados en una custom cache en un mapa. Donde por cada
web request buscamos primero en la Cache con datos deterministicos y si no existen
en esta buscamos en la cache de datos probabilisticos.


 